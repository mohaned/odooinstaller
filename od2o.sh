#!/bin/bash

valid_version="8 9"

# Check if this script is not running as root
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

# Check if version is specified
if [[ "$#" -ne 1 ]]; then
    echo "Please specify version of odoo !"
    echo "Usage: sudo $0 8"
    echo "or\nUsage: sudo $0 9"
    exit 1
fi

# Check if it's valid version
if [[ $valid_version =~ $1 ]]; then
	wget -O - https://nightly.odoo.com/odoo.key | apt-key add -
	# Add odoo
	echo "deb http://nightly.odoo.com/$1.0/nightly/deb/ ./" >> /etc/apt/sources.list
	# Update and install
	apt-get update && apt-get install odoo
	# replace exit 0 with iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 8069 in /etc/rc.local
	sed -i 's/^exit 0$/iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 8069\nexit 0/g' /etc/rc.local
else
	echo "Only version 8 and 9 are supported!"
	exit 1
fi

